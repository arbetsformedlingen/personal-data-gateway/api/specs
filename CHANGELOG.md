# Changelog

All notable changes to this project will be documented in this file. See [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version) for commit guidelines.

## [4.1.1](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/compare/v4.1.0...v4.1.1) (2024-12-27)


### Bug Fixes

* add 503 Service Unavailable to the health endpoint ([2d5248c](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/commit/2d5248c123a3328227a4a0c14c72f9591865cff2))

## [4.1.0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/compare/v4.0.0...v4.1.0) (2024-11-25)


### Features

* add pagination to get collection endpoints ([d1c0758](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/commit/d1c0758ab4783939590f8e30a54dc98c73176ca2))

## [4.0.0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/compare/v3.3.1...v4.0.0) (2024-11-19)


### ⚠ BREAKING CHANGES

* remove deprecated response headers: "X-RateLimit-Limit", "X-RateLimit-Remaining", and "X-RateLimit-Reset". Also Removed unused schemas.
* remove deprecated /datatypes endpoints

### Features

* remove deprecated /datatypes endpoints ([eb599c3](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/commit/eb599c3ebe0ae00619d5a39429d2b301ea661062))
* remove deprecated response headers: "X-RateLimit-Limit", "X-RateLimit-Remaining", and "X-RateLimit-Reset". Also Removed unused schemas. ([d02df2a](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/commit/d02df2a498fb306111aeb4c5a8d0def4dc9be860))

## [3.3.1](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/compare/v3.3.0...v3.3.1) (2024-11-12)


### Bug Fixes

* add buildVersion to GetApiInfoResponse ([4ec92c4](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/commit/4ec92c4581d38db96595bb3997b3762363fc7c24))

## [3.3.0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/compare/v3.2.1...v3.3.0) (2024-11-07)


### Features

* Use RateLimit-Policy and RateLimit response headers in all API endpoint responses. Deprecated X-RateLimit-Limit, X-RateLimit-Remaining and X-RateLimit-Reset response headers ([49159c7](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/commit/49159c71978d0ea524199441bb321232432e6605))

## [3.2.1](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/compare/v3.2.0...v3.2.1) (2024-11-05)


### Bug Fixes

* change ApiStatus to Status for the health endpoint ([613f6e8](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/commit/613f6e8dfb0d5d2c88862343bfab9258c23253b7))

## [3.2.0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/compare/v3.1.1...v3.2.0) (2024-11-04)


### Features

* add api-info endpoint ([5cad106](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/commit/5cad10699363b75f24a37059a889ebd46c311083))
* add error codes 406 and 451 to the data endpoints ([446520f](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/commit/446520f360531913d474e9d2ad71c97b25d31ba3))

## [3.1.1](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/compare/v3.1.0...v3.1.1) (2024-10-07)


### Bug Fixes

* typo in operationId GetDatasourceeFaq ([582a3f5](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/commit/582a3f57c4354400c46e72e97d183f69346cccd2))

## [3.1.0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/compare/v3.0.0...v3.1.0) (2024-10-03)


### Features

* datasources can now have faqs, and deprecated all datatype endpoints. ([db44e2f](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/commit/db44e2f0aeef8023cb545ef0ab972561518641fb))


### Bug Fixes

* increase max length to 64 for date-time properties. Previous max length of 20 characters prematurely truncated the date-time value. ([61a06d9](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/commit/61a06d925b6247b5bd87b81044a3186594a1a84c))

## [3.0.0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/compare/v2.0.0...v3.0.0) (2024-09-09)


### ⚠ BREAKING CHANGES

* the Sharing schemas now have property "datasourceId" instead of "datatypeUrl"
* POST /users/{user-id}/sharings now returns "200 - OK" if a similar active sharing already exists, otherwise "201 - Created" if a new sharing was created
* POST /users/{user-id}/sharings now returns "200 - OK" if a similar active sharing already exists, otherwise "201 - Created" if a new sharing was created

### Features

* POST /users/{user-id}/sharings now returns "200 - OK" if a similar active sharing already exists, otherwise "201 - Created" if a new sharing was created ([76205f1](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/commit/76205f193dfbf61d2bacbd1f1c1070b90412928a))
* POST /users/{user-id}/sharings now returns "200 - OK" if a similar active sharing already exists, otherwise "201 - Created" if a new sharing was created ([0885ae7](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/commit/0885ae7d227f230fd3096f0a869af500e53855c8))
* the Sharing schemas now have property "datasourceId" instead of "datatypeUrl" ([84ece60](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/commit/84ece60dcaf67b6dd3e77bb3bc443495048092f9))

## [2.0.0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/compare/v1.0.0...v2.0.0) (2024-08-14)


### ⚠ BREAKING CHANGES

* remove apiKey property from PatchClient request body

### Features

* add POST and GET client key resets endpoint ([866b2a2](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/commit/866b2a2f20b47685179207be5268a860dcdc6357))
* remove apiKey property from PatchClient request body ([f176a1f](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/commit/f176a1faa93f29f9ea320a7be771c4bb120695da))


### Bug Fixes

* add a clientKeyResetId property to POST and GET client key resets endpoint responses ([e5e497f](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/commit/e5e497fb3de2e6d4b34e38ad0171055ca1a8689b))
* set clientKeyResetId to be mandatory for POST and GET client key resets endpoint ([5f67c78](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/commit/5f67c78ce00e1862af193f8ef9ac233718312af7))

## [1.0.0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/compare/v0.15.1...v1.0.0) (2024-05-31)

## [0.15.1](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/compare/v0.15.0...v0.15.1) (2024-05-31)


### Bug Fixes

* remove clientId from post request, add redirectId to responses ([7f8396e](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/commit/7f8396e8f1e3d51b4164a6e30bf60b7c6883d79b))

## [0.15.0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/compare/v0.14.0...v0.15.0) (2024-05-30)


### ⚠ BREAKING CHANGES

* drop "redirectUrls" property from Client schemas.

### Features

* add endpoints "POST & GET /clients/{client-id}/redirects" and "GET & PATCH /clients/{client-id}/redirects/{redirect-id}". ([a2a0734](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/commit/a2a0734a93227bfe93c0c53203cf03e420b0f48c))
* drop "redirectUrls" property from Client schemas. ([6f2676b](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/commit/6f2676ba6c2564c9a958e519fccc1edf2215c10b))

## [0.14.0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/compare/v0.13.1...v0.14.0) (2024-05-21)


### ⚠ BREAKING CHANGES

* replace "orgNr" with "organizationId" in all "client" operations.

### Bug Fixes

* rename "humanFriendlyName" to "name" in all client operations. ([0642707](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/commit/0642707e80a0e422fb354332dfa841d992f74b09))
* replace "orgNr" with "organizationId" in all "client" operations. ([84e6821](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/commit/84e682158991282fae0c157ebf4f26b5cbc7dd7a))

## [0.13.1](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/compare/v0.13.0...v0.13.1) (2024-05-21)


### Bug Fixes

* add "deleted" property to "PATCH /organizations/{organization-id}/faqs/{faq-id}" request ([f14fdf6](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/commit/f14fdf6fc70d01511ed4af492d5446962ba4f009))
* add possible "409 - Conflict" response status to PATCH /organizations/{organization-id}/faqs/{faq-id} endpoint ([0f28a4a](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/commit/0f28a4aaa104d31c0a44bf1a2c0bfcf6f31ab1a1))

## [0.13.0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/compare/v0.12.4...v0.13.0) (2024-05-20)


### ⚠ BREAKING CHANGES

* require "updated"-property for all responses, added "deleted"- and "revoked"-properties where missing.

### Bug Fixes

* correct the possible response status codes (404 and 409) related to organizations and datatypes endpoints ([ae73351](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/commit/ae7335138f1f8593f37f60973fd1b5b3d7e1261b))
* remove description about "Accept-Language" from endpoint "GET /datatypes" ([42894c9](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/commit/42894c900e6d5291e960a89297f26a53a1ac68b9))
* require "updated"-property for all responses, added "deleted"- and "revoked"-properties where missing. ([20dc5ce](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/commit/20dc5ce237c43f6f203a22e796575927a01d2a2e))
* return the correct PatchOrganizationFaq response for the PATH organization FAQ request ([50aa115](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/commit/50aa1156d6ad02a61fa0e45afaf3b1cae961c56c))

## [0.12.4](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/compare/v0.12.3...v0.12.4) (2024-05-07)


### Features

* Add HATEOAS Link response header to relevant operations ([f2499a2](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/commit/f2499a291139ab61dddb555f8570bd305b74419e))

## [0.12.3](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/compare/v0.12.2...v0.12.3) (2024-05-07)


### Bug Fixes

* remove 'Accept-Language' from GET organizations and GET datatypes endpoints ([2a5d273](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/commit/2a5d2735b21965ba3a30dff6ddd76ebafbaa9526))

## [0.12.2](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/compare/v0.12.1...v0.12.2) (2024-05-06)

## [0.12.1](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs/compare/v0.12.0...v0.12.1) (2024-04-29)
