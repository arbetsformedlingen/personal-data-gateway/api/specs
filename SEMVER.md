# Semantic versioning

The releases of this repository follows [Semantic Versioning 2.0.0](https://semver.org/spec/v2.0.0.html).

Changes in any of the following files defines the semantic version of releases:

- [openapi.yaml](openapi.yaml)
- [LICENSE.md](LICENSE.md)
